import ayar
import sabit
import time
import requests
import json

from threading import Thread
from client import Client
from datetime import datetime
from datetime import timedelta


class Tacir:
    def __init__(self, isim):
        # Parametreler
        self.isim = isim
        self.client = None
        self.opsiyon1 = ""
        self.opsiyon2 = ""
        self.piyasa = ""
        self.katSayi = 1.0
        self.alimPeriyodu = 0.01
        self.satimPeriyodu = 0.006
        self.piyasaSorguSuresi = 1
        self.ilkAlim = 11.0
        self.ondalikFormat = ""
        self.durum = None
        self.adimKatsayisi = 1.0
        self.maxAlimSayisi = 100

        # Değerler
        self.kar = 0.0
        self.mevcutAdet = 0.0
        self.mevcutMaliyet = 0.0
        self.alimYuzdeCizgisi = 1.0
        self.satimYuzdeCizgisi = 1.0
        self.satimBayragi = False
        self.origin = 1.0
        self.alinacakMiktar = 0.0
        self.alimSayisi = 0
        self.baslamaZamani = None
        self.bitirmeZamani = None
        self.bakiye = 0
        self.donemKodu = ""
        self.alimHatasiKilit = False
        self.satimHatasiKilit = False
        self.alimSiniriHatasiKilit = False
        self.baglan()

    def baglan(self):
        time.sleep(3)
        try:
            self.client = Client(ayar.binanceKey, ayar.binanceSecret)
        except:
            print("Binance'e bağlanılamadı.")

    def parametreOku(self):
        url = ayar.appScriptBase+"?action=read&table=Parametre"
        while(True):
            try:
                response = requests.get(url)
                tacirler = response.json()
                for tacir in tacirler["data"]:
                    if tacir["isim"] == self.isim:
                        self.durum = tacir["durum"]
                        self.opsiyon1 = tacir["opsiyon1"]
                        self.opsiyon2 = tacir["opsiyon2"]
                        self.piyasa = tacir["opsiyon1"]+tacir["opsiyon2"]
                        self.alimPeriyodu = tacir["alimPeriyodu"]
                        self.satimPeriyodu = tacir["satimPeriyodu"]
                        self.katSayi = tacir["katSayi"]
                        self.ilkAlim = tacir["ilkAlim"]
                        self.piyasaSorguSuresi = tacir["piyasaSorguSuresi"]
                        self.adimKatsayisi = tacir["adimKatsayisi"]
                        self.maxAlimSayisi = tacir["maxAlimSayisi"]
                break
            except:
                print("Parametre Okunamadı.")
                time.sleep(3)

    def degerleriSifirla(self):
        self.mevcutAdet = 0.0
        self.mevcutMaliyet = 0.0
        self.alimYuzdeCizgisi = 1.0
        self.satimYuzdeCizgisi = 1.0
        self.satimBayragi = False
        self.origin = 1.0
        self.alinacakMiktar = 0.0
        self.alimSayisi = 0
        self.kar = 0.0
        self.baslamaZamani = None
        self.bitirmeZamani = None
        self.bakiye = 0
        self.satimFiyatı = 0.0
        self.alimHatasiKilit = False
        self.satimHatasiKilit = False
        self.adimKatsayisi = 1.0
        self.maxAlimSayisi = 100
        self.alimSiniriHatasiKilit = False

    def ondalikFormatGuncelle(self):
        adimBuyuklugu = ""
        while(True):
            try:
                piyasaBilgisi = self.client.get_symbol_info(self.piyasa)
                for filter in piyasaBilgisi["filters"]:
                    if filter["filterType"] == "LOT_SIZE":
                        adimBuyuklugu = filter["stepSize"]

                hassasiyet = 0
                carpan = 1
                while(True):
                    if(float(adimBuyuklugu)*carpan == 1.0):
                        break
                    carpan = carpan*10
                    hassasiyet = hassasiyet+1

                self.ondalikFormat = "0."+str(hassasiyet)+"f"
                break
            except:
                print("Ondalık Format Güncellenemedi.")
                self.baglan()

    def piyasaFiyati(self):
        fiyat = 0.0
        while(True):
            try:
                fiyat = self.client.get_symbol_ticker(
                    symbol=self.piyasa)["price"]
                break
            except:
                print("Piyasa Fiyatı Alınamadı.")
                self.baglan()
        return float(fiyat)

    def alimSayisiArtir(self):
        self.alimSayisi = self.alimSayisi+1

    def tacirAktifMi(self):
        durum = False
        if self.durum == "Aktif":
            durum = True
        return durum

    def ilkAlimGuncelle(self, fiyat):
        miktar = self.ilkAlim/fiyat
        self.alinacakMiktar = float(format(miktar, self.ondalikFormat))

    def zamanBaslat(self):
        self.baslamaZamani = self.turkiyeSaati()

    def zamanBitir(self):
        self.bitirmeZamani = self.turkiyeSaati()

    def alinacakMiktarGuncelle(self):
        self.alinacakMiktar = float(
            format(self.alinacakMiktar*self.katSayi, self.ondalikFormat))

    def alimYuzdeCizgisiGuncelle(self):
        self.alimYuzdeCizgisi = self.alimYuzdeCizgisi - self.alimPeriyodu

    def alimKaydet(self, alimData):
        alimAdet = 0.0
        alimFiyat = 0.0
        for alimAdim in alimData["fills"]:
            adimFiyat = float(alimAdim["price"])
            adimAdet = float(alimAdim["qty"])
            alimFiyat = (alimAdet*alimFiyat+adimAdet *
                         adimFiyat)/(alimAdet+adimAdet)
            alimAdet = alimAdet+adimAdet

        self.mevcutMaliyet = (self.mevcutAdet * self.mevcutMaliyet +
                              alimAdet * alimFiyat)/(self.mevcutAdet+alimAdet)

        self.mevcutAdet = float(
            format(self.mevcutAdet+alimAdet, self.ondalikFormat))

        hedefFiyat = self.hedefHesapla()
        aMaliyet = float(format(alimFiyat, "0.8f"))
        mMaliyet = float(format(self.mevcutMaliyet, "0.8f"))

        print("________________________________________")
        print("                 %s.Alım                 " % (self.alimSayisi))
        print("________________________________________")
        print("Piyasa           :%s" % (self.piyasa))
        print("Alım Adet        :%s" % (alimAdet))
        print("Alım Fiyat       :%s" % (aMaliyet))
        print("Mevcut Adet      :%s" % (self.mevcutAdet))
        print("Mevcut Maliyet   :%s" % (mMaliyet))
        print("Hedef            :%s" % (hedefFiyat))
        print("________________________________________")
        self.alimMesajiGonder(alimAdet, alimFiyat)
        self.alimLogKaydet(alimAdet, aMaliyet)

    def hedefHesapla(self):
        hedefFiyat = (1.0+self.satimPeriyodu)*self.mevcutMaliyet
        hedefFiyat = float(format(hedefFiyat, "0.8f"))
        return hedefFiyat

    def coinAl(self):
        result = False
        if(self.maxAlimSayisi == self.alimSayisi):
            if(not self.alimSiniriHatasiKilit):
                self.alimSiniriHatasiMesajiGonder()
                self.alimSiniriHatasiKilit = True
            print('!A-SINIR', end='', flush=True)
            time.sleep(30)
            return result

        try:
            result = self.client.order_market_buy(
                symbol=self.piyasa, quantity=self.alinacakMiktar)
            self.alimHatasiKilit = False
        except:
            # Alım Hatası
            if(self.alimHatasiKilit == False):
                self.alimHatasiMesajiGonder()
                self.alimHatasiKilit = True
            print('!A', end='', flush=True)
            time.sleep(30)
            self.ondalikFormatGuncelle()
            self.alinacakMiktar = float(
                format(self.alinacakMiktar, self.ondalikFormat))
            self.baglan()
        return result

    def coinSat(self):
        result = False
        try:
            result = self.client.order_market_sell(
                symbol=self.piyasa, quantity=self.mevcutAdet)
            self.satimHatasiKilit = False
        except:
            # Satım Hatası
            if(self.satimHatasiKilit == False):
                self.satimHatasiMesajiGonder()
                self.satimHatasiKilit = True
            print('!S', end='', flush=True)
            self.ondalikFormatGuncelle()
            self.mevcutAdet = float(
                format(self.mevcutAdet, self.ondalikFormat))
            self.baglan()
        return result

    def satimKaydet(self, satimData):
        hamAdet = 0.0
        hamFiyat = 0.0
        for satimAdim in satimData["fills"]:
            adimFiyat = float(satimAdim["price"])
            adimAdet = float(satimAdim["qty"])
            hamFiyat = (hamAdet*hamFiyat+adimAdet*adimFiyat)/(hamAdet+adimAdet)
            hamAdet = hamAdet+adimAdet
        self.satimFiyatı = hamFiyat
        kar = (hamFiyat-self.mevcutMaliyet)*self.mevcutAdet-(hamFiyat *
                                                             self.mevcutAdet*0.00075)-(self.mevcutMaliyet*self.mevcutAdet*0.00075)
        self.kar = float(format(kar, "0.2f"))

        print("KAR  :%s" % (self.kar))

    def zamanHesapla(self):
        zamanFarki = self.bitirmeZamani-self.baslamaZamani
        return str(zamanFarki).split(".")[0]

    def alimHatasiMesajiGonder(self):
        tutar = self.origin*(self.alimYuzdeCizgisi -
                             self.alimPeriyodu)*self.alinacakMiktar
        tutar = float(format(tutar, "0.2f"))
        metin = sabit.alimHatasiTaslak % (self.isim, self.piyasa, tutar)
        self.mesajGonder(sabit.telegramTacirBotId,
                         ayar.telegramChatId, metin)

    def alimSiniriHatasiMesajiGonder(self):
        metin = sabit.alimSiniriHatasiTaslak % (
            self.isim, self.piyasa, self.maxAlimSayisi)
        self.mesajGonder(sabit.telegramTacirBotId,
                         ayar.telegramChatId, metin)

    def satimHatasiMesajiGonder(self):
        metin = sabit.satimHatasiTaslak % (
            self.isim, self.piyasa, self.mevcutAdet)
        self.mesajGonder(sabit.telegramTacirBotId,
                         ayar.telegramChatId, metin)

    def donemMesajiGonder(self):
        metin = sabit.donemTaslak % (
            self.isim, self.piyasa, self.alimSayisi, self.zamanHesapla(), self.kar, self.bakiye, self.satimYuzdeCizgisi)

        self.mesajGonder(sabit.telegramTacirBotId,
                         ayar.telegramChatId, metin)

    def alimMesajiGonder(self, alimAdet, alimFiyat):
        hedefFiyat = (1.0+self.satimPeriyodu)*self.mevcutMaliyet
        hedefFiyat = float(format(hedefFiyat, "0.8f"))
        aMaliyet = float(format(alimFiyat, "0.8f"))
        mMaliyet = float(format(self.mevcutMaliyet, "0.8f"))

        metin = sabit.alimTaslak % (self.isim, self.piyasa, self.alimSayisi,
                                    alimAdet, aMaliyet, self.mevcutAdet, mMaliyet, hedefFiyat)
        self.mesajGonder(sabit.telegramAlimBotId,
                         ayar.telegramChatId, metin)

    def mesajGonder(self, botId, chatId, mesaj):
        url = 'https://api.telegram.org/'+botId+'/sendMessage'
        content = {'chat_id': chatId, 'text': mesaj, 'parse_mode': "markdown"}

        while(True):
            try:
                Thread(target=requests.post, args=[url, content]).start()
                break
            except:
                print("Mesaj Gönderilemedi.")

    def analiz(self, simdikiFiyat):
        if(simdikiFiyat > self.mevcutMaliyet):
            print('+', end='', flush=True)
        else:
            print('-', end='', flush=True)

    def satirDuzelt(self):
        print("")

    def bakiyeHesapla(self):
        toplamBakiye = 0
        while(True):
            try:
                tumPiyasalar = self.client.get_all_tickers()
                bakiyelerim = self.client.get_account()["balances"]

                for bakiye in bakiyelerim:
                    if(float(bakiye["free"]) > 0):
                        if(bakiye["asset"] == "USDT"):
                            toplamBakiye = toplamBakiye + float(bakiye["free"])
                        else:
                            for piyasa in tumPiyasalar:
                                if(piyasa["symbol"] == bakiye["asset"]+"USDT"):
                                    hesaplanan = float(
                                        piyasa["price"])*float(bakiye["free"])
                                    toplamBakiye = toplamBakiye + hesaplanan
                break
            except Exception as e:
                print(e)
                print("Bakiye Hesaplanamadı")
                self.baglan()
        self.bakiye = float(format(toplamBakiye, "0.2f"))

    def alimLogKaydet(self, miktar, fiyat):
        data = {
            "Alım Zamanı": self.turkiyeSaati().strftime("%Y.%m.%d %H:%M:%S"),
            "Tacir": self.isim,
            "Dönem Kodu": self.donemKodu,
            "Piyasa": self.piyasa,
            "Alım No": self.alimSayisi,
            "Alım Miktarı": miktar,
            "Alım Fiyatı": fiyat,
            "Maliyet": float(format(self.mevcutMaliyet, "0.8f")),
            "Hedef": self.hedefHesapla()
        }
        self.logKaydet("Alim", data)

    def donemLogKaydet(self):
        data = {
            "Başlama Zamanı": self.baslamaZamani.strftime("%Y.%m.%d %H:%M:%S"),
            "Bitiş Zamanı": self.bitirmeZamani.strftime("%Y.%m.%d %H:%M:%S"),
            "Tacir": self.isim,
            "Dönem Kodu": self.donemKodu,
            "Piyasa": self.piyasa,
            "Alım Sayısı": self.alimSayisi,
            "Miktar": float(format(self.mevcutAdet, "0.8f")),
            "Maliyet": float(format(self.mevcutMaliyet, "0.8f")),
            "Satım Fiyatı": float(format(self.satimFiyatı, "0.8f")),
            "Kar": float(format(self.kar, "0.8f")),
            "Bakiye": float(format(self.bakiye, "0.8f"))
        }
        self.logKaydet("Donem", data)

    def logKaydet(self, sayfa, data):
        veri = json.dumps(data)
        url = ayar.appScriptBase+"?action=insert&table="+sayfa+"&data="+veri

        while(True):
            try:
                Thread(target=requests.get, args=[url]).start()
                break
            except:
                print("Log Kaydedilemedi.")

    def donemKoduGuncelle(self):
        zaman = self.baslamaZamani.strftime("%Y%m%d%H%M%S")
        self.donemKodu = self.isim + "-" + zaman

    def turkiyeSaati(self):
        simdi = datetime.utcnow() + timedelta(hours=3)
        return simdi

    def basla(self):
        while(True):
            self.degerleriSifirla()
            self.parametreOku()
            if not self.tacirAktifMi():
                print("Tacir Pasif Durumda.")
                time.sleep(20)
                continue
            self.ondalikFormatGuncelle()
            self.ilkAlimGuncelle(self.piyasaFiyati())
            self.zamanBaslat()
            self.donemKoduGuncelle()

            while self.tacirAktifMi():
                if self.mevcutAdet == 0.0:
                    # İlk İşlem
                    alimData = self.coinAl()
                    if not alimData:
                        continue
                    self.alimSayisiArtir()
                    self.alimKaydet(alimData)

                    self.origin = self.mevcutMaliyet
                    self.alinacakMiktarGuncelle()
                else:
                    # Sonraki İşlemler
                    simdikiFiyat = self.piyasaFiyati()
                    self.analiz(simdikiFiyat)

                    # Satım Kilidi Açıldı. Çizgi Yukarı Çekildi.
                    if simdikiFiyat > self.mevcutMaliyet * (self.satimYuzdeCizgisi+self.satimPeriyodu):
                        self.satimBayragi = True
                        self.satimYuzdeCizgisi = self.satimYuzdeCizgisi + self.satimPeriyodu
                        print("Satım Çizgisi güncellendi :" +
                              str(self.satimYuzdeCizgisi))

                    # Satıma Uygun
                    if self.satimBayragi:
                        if simdikiFiyat < self.mevcutMaliyet * (self.satimYuzdeCizgisi*1.0):
                            satimData = self.coinSat()
                            if not satimData:
                                continue
                            self.satirDuzelt()
                            self.zamanBitir()
                            self.durum = "Pasif"
                            self.bakiyeHesapla()
                            self.satimKaydet(satimData)
                            self.donemMesajiGonder()
                            self.donemLogKaydet()
                    elif simdikiFiyat < self.origin*(self.alimYuzdeCizgisi-self.alimPeriyodu):
                        # Alıma Uygun
                        alimData = self.coinAl()
                        if not alimData:
                            continue
                        self.satirDuzelt()
                        self.alimSayisiArtir()
                        self.alimKaydet(alimData)
                        self.alinacakMiktarGuncelle()
                        self.alimYuzdeCizgisiGuncelle()
                        self.alimPeriyodu = self.alimPeriyodu * self.adimKatsayisi
                # İki Piyasa Fiyatı Öğrenme Arası Bekleme
                time.sleep(self.piyasaSorguSuresi)
